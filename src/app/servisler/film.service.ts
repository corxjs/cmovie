import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FilmService {

  constructor(private http:HttpClient) { }
  
 filmAra(baslik:string){
    
   console.log(baslik+ `servis \nhttps://www.themoviedb.org/search/multi?language=tr-TR&query=${baslik}&api_key=8691f53a75cc57f2be98fe1eb0db941f`);
   return this.http.get(`https://api.themoviedb.org/3/search/multi?language=tr-TR&query=${baslik}&api_key=8691f53a75cc57f2be98fe1eb0db941f`);
  }
}
