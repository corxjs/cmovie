export class Film {
    //https://image.tmdb.org/t/p/w300_and_h450_bestv2/l7zANdjgTvYqwZUx76Vk0EKpCH5.jpg Resim url yapısı
    title:string; //başlık
    media_type:string; //Tip 
    poster_path:string; //Görsel
    id:number; //id
    original_language:string; //orjinal dil
    original_title:string; //Orjinal adı
    overview:string; //Açıklama
    release_date:string; //Çıkış tarihi
    popularity:number;
}
