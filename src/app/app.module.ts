import { Film } from './models/film';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AnasayfaComponent } from './components/anasayfa/anasayfa.component';
import { JumbotronComponent } from './components/anasayfa/jumbotron/jumbotron.component';


@NgModule({
  declarations: [
    AppComponent,
    AnasayfaComponent,
    JumbotronComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [
    Film
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
