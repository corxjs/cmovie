import { FilmService } from './../../servisler/film.service';
import { Film } from './../../models/film';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-anasayfa',
  templateUrl: './anasayfa.component.html',
  styleUrls: ['./anasayfa.component.css']
})
export class AnasayfaComponent implements OnInit {

  constructor(private filmServis:FilmService) { }
  filmler:Film[]=[];
  ngOnInit() {
  }
  
  getArananFilm(baslik:string){
    console.log("dada");
   this.filmServis.filmAra(baslik).subscribe((resp:any)=>{
     console.log(resp.results);
     
    this.filmler = resp.results;
    
    console.log(this.filmler);
    for(let i of resp.results){
        i.poster_path="https://image.tmdb.org/t/p/w300_and_h450_bestv2"+i.poster_path;
        console.log(i.poster_path.substring(i.poster_path.length-4));
    }
   
    
   });
  }
}
